import uuid
import datetime

from routes.base import BaseHandler


class SigninHandler(BaseHandler):
    def get(self):
        if self.get_cookie("user"):
            self.redirect("/")
            return
        self.render("auth/signin.html")

    def post(self):
        email = self.get_argument("email")
        password = self.get_argument("password")

        if not self.valid_email(email):
            self.set_flash("error", "Please enter a valid email")
            self.redirect("/user/signin")
            return

        user = self.application.db.users.find_one({"profile.email": email})
        hashed_password = self.get_password(password)
        if user and (user["profile"]["password"] == hashed_password):
            self.set_current_user(user)
            self.redirect("/")
            return
        else:
            self.set_flash("error", "Wrong email and password combination")
            self.redirect("/user/signin")

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", str(user["_id"]), expires_days=1)
        else:
            self.clear_cookie("user")


class SignupHandler(BaseHandler):
    def get(self):
        if self.get_cookie("user"):
            self.redirect("/")
            return
        self.render("auth/signup.html")

    def post(self):
        form_fields = ["name", "email", "password", "password_confirmation"]
        for field in form_fields:
            if self.empty_field(self.get_argument(field)):
                self.set_flash("error", "All fields are required")
                self.redirect("/user/signup")
                return

        if self.user_exists(self.get_argument("email")):
            self.set_flash("info",
                           "You already have an account with us. Please login or reset your password if you're having trouble logging in")
            self.redirect("/user/signin")
            return

        if not self.valid_email(self.get_argument("email")):
            self.set_flash("error", "Please enter a valid email")
            self.redirect("/user/signup")
            return

        if not self.valid_password(self.get_argument("password")):
            self.set_flash("error", "Password must be at least 6 characters long")
            self.redirect("/user/signup")
            return

        if not self.confirm_password(self.get_argument("password"), self.get_argument("password_confirmation")):
            self.set_flash("error", "Password and confirmation do not match")
            self.redirect("/user/signup")
            return

        user = {"profile": {"created_on": datetime.datetime.now(), "updated_on": datetime.datetime.now()},
                "settings": {}}

        profile_fields = ["name", "email"]

        for key in profile_fields:
            user["profile"][key] = self.get_argument(key)
        hashed_password = self.get_password(self.get_argument("password"))
        user["profile"]["password"] = hashed_password

        uid = self.db.users.insert(user)
        if uid:
            self.mailer.new_user(user["profile"]["email"])
            self.set_secure_cookie("user", str(uid), expires_days=1)
            self.redirect("/")
            return
        else:
            self.set_flash("error", "Your account couldn't be created! Please try again later")
            self.redirect(self.get_argument("next", "/user/signin"))


class SignoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect("/user/signin")


class UserForgotHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.render("auth/forgot.html")

    def post(self):
        email = self.get_argument("email")

        if not self.valid_email(email):
            self.set_flash("error", "Please enter a valid email")
            self.redirect("/user/forgot")
            return

        if not self.user_exists(self.get_argument("email")):
            self.set_flash("info", "You don't have an account with us. Please sign up below")
            self.redirect("/user/signup")
            return

        code = uuid.uuid4().hex
        self.db.resets.insert({"email": email, "code": code, "created_on": datetime.datetime.now()})
        # TODO: enable this mailer
        # if self.mailer.reset_password(email, code):
        #     self.set_flash("info", "We've emailed you a link to reset your password. Follow the instructions there")
        #     self.redirect("/user/signin")
        # else:
        #     self.set_flash("error", "We failed to send you a reset email. Please try again later")
        #     self.redirect("/user/forgot")


class UserForgotResetHandler(BaseHandler):
    def get(self, code):
        exists = self.db.resets.find_one({"code": code})
        if exists and not self.expired_reset_link(exists["created_on"]):
            email = exists["email"]
            self.render("auth/forgot_reset.html", email=email)
        else:
            self.set_flash("error", "Wrong or expired password reset link. Reset your password again")
            self.redirect("/user/forgot")

    def post(self, code):
        email = self.get_argument("email")

        if not self.valid_password(self.get_argument("password")):
            self.set_flash("error", "Password must be at least 6 characters")
            self.redirect("/user/forgot/" + code)
            return

        if not self.confirm_password(self.get_argument("password"), self.get_argument("password_confirmation")):
            self.set_flash("error", "Password and confirmation do not match")
            self.redirect("/user/forgot/" + code)
            return

        user = self.db.users.find_one({"profile.email": email})
        hashed_password = self.get_password(self.get_argument("password"))
        user["profile"]["password"] = hashed_password
        self.db.users.save(user)
        self.db.resets.remove(self.db.resets.find_one({"email": email}))
        self.set_flash("success", "Your password has been reset. You can login below")
        self.redirect("/user/signin")