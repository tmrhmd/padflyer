import tornado.web

from bson import ObjectId
from routes.base import BaseHandler


class DashboardHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        response = {
            "properties": self.db.properties.find({"user_id": self.current_user["_id"]}).count(),
            "units": self.db.units.find({"user_id": self.current_user["_id"]}).count(),
            "leads": self.db.leads.find({"user_id": self.current_user["_id"]}).count(),
            "flyers":self.db.flyers.find({"user_id": self.current_user["_id"]}).count()
        }
        self.respond(response)


class PropertyHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, oid=None):
        if oid:
            if not ObjectId.is_valid(oid):
                self.respond({"code": 400, "message": "Invalid property parameter"})
            response = self.db.properties.find({"_id": ObjectId(oid), "user_id": self.current_user["_id"]})
            if response.count() > 0:
                self.respond(response)
            self.respond({"code": 404, "message": "Property not found!"})
        response = self.db.properties.find({"user_id": self.current_user["_id"]})
        self.respond(response)

    @tornado.web.authenticated
    def post(self, oid=None):
        request = self.receive(self.request.body)
        request["user_id"] = self.current_user["_id"]
        oid = self.db.properties.insert(request)
        self.set_status(201)
        self.respond({"id": str(oid)})

    @tornado.web.authenticated
    def put(self, oid=None):
        self.finish("I'm PUT")

    @tornado.web.authenticated
    def delete(self, oid):
        if not ObjectId.is_valid(oid):
            self.respond({"code": 400, "message": "Invalid property parameter"})
        doc = self.db.properties.find_one({"_id": ObjectId(oid), "user_id": self.current_user["_id"]})
        self.db.units.remove({"property_id": doc["_id"]})
        self.db.properties.remove(doc)
        self.respond({"code": "200", "message": "Property demolished!"})