# !/usr/bin/env python3

import os

import tornado.ioloop
import tornado.httpserver
import tornado.web

import routes.auth
import routes.base
import routes.api

from utils.mailer import Mailer
from pymongo import MongoClient
from tornado.options import define, options, parse_command_line
from setup import setup_environment


define("port", default=3000, type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/user/signin/?", routes.auth.SigninHandler),
            (r"/user/signup/?", routes.auth.SignupHandler),
            (r"/user/signout/?", routes.auth.SignoutHandler),
            (r"/user/forgot/?", routes.auth.UserForgotHandler),
            (r"/user/forgot/([0-9a-f]{32})/?", routes.auth.UserForgotResetHandler),
            (r"/", routes.base.IndexHandler),
            (r"/api/dashboard", routes.api.DashboardHandler),
            (r"/api/properties/?(.*)", routes.api.PropertyHandler),
        ]
        settings = dict(
            app_name="Pad Flyer",
            static_path=os.path.join(os.path.dirname(__file__), "public"),
            template_path=os.path.join(os.path.dirname(__file__), "public", "html"),
            ui_modules=dict(FlashModule=routes.base.FlashModule),
            mailgun_url=os.getenv("PADFLYER_MAILGUN_URL"),
            mailgun_secret=os.getenv("PADFLYER_MAILGUN_SECRET"),
            password_secret=os.getenv("PADFLYER_PASSWORD_SECRET"),
            cookie_secret=os.getenv("PADFLYER_COOKIE_SECRET"),
            login_url="/user/signin",
            debug=True
        )
        tornado.web.Application.__init__(self, handlers, **settings)

        if self.settings.get("debug", ''):
            self.dbc = MongoClient(os.getenv("PADFLYER_DATABASE_URL"))
        else:
            self.dbc = MongoClient(os.getenv("PADFLYER_DATABASE_URL"))
        self.db = self.dbc["padflyer-dev"]
        self.mailer = Mailer(self.settings["mailgun_secret"], self.settings["mailgun_url"])


def main():
    try:
        setup_environment()
        parse_command_line()
        http_server = tornado.httpserver.HTTPServer(Application())
        http_server.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        print("\nStopping application...\n")


if __name__ == "__main__":
    main()