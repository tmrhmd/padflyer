import tornado.gen
import urllib.parse
import tornado.httpclient


class Mailer(object):
    def __init__(self, api_key, base_url):
        self.API_KEY = api_key
        self.BASE_URL = base_url
        self.client = tornado.httpclient.AsyncHTTPClient()

    @tornado.gen.coroutine
    def send(self, data):
        data = urllib.parse.urlencode(data)
        request = tornado.httpclient.HTTPRequest(
            self.BASE_URL + "/messages",
            "POST",
            auth_username="api",
            auth_password=self.API_KEY,
            body=data
        )
        response = yield self.client.fetch(request)
        if response.code == 200:
            return True
        return False

    def new_user(self, email):
        data = {
            "from": "Pad Flyer <no-reply@mailer.padflyer.com>",
            "to": email,
            "subject": "Welcome to Pad Flyer!",
            "text": "Thanks for signing up!"
        }
        if self.send(data):
            return True
        return False